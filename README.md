# Comandos Básicos GIT - ATT
## 1. Configuración GIT para acceso por SSH (Requerido)
#### * Esta configuración se realizará para los entornos de Trabajo GITLAB y GIT.TI
#### Para tener configurado el SSH - GIT es necesario:
	a. Crear el archivo "config" dentro de la carpeta .ssh:
        -------------------
        # GitLab | Personal
            Host juanlimber.gitlab.com
            Host gitlab.com
            Preferredauthentications publickey
            IdentityFile ~/.ssh/gitlab_rsa
        # GitLab | Work
        Host juanlimber.git.ti
            Host git.ti
            Preferredauthentications publickey
            IdentityFile ~/.ssh/gitti_rsa
        -------------------
	b. Crear las llaves ssh (privada-pública):
		ssh-keygen -t rsa -b 4096 -C "jchoque@att.gob.bo"
        -> Nombre del archivo: example_rsa
	c. Subir el archivo example_rsa.pub en el server gitlab ó git.ti.
	d. Probar la conexión:
		ssh -T git@gitlab.com
		ssh -T git@git.ti

## 2. Versionar Código
#### Clonar el proyecto desde el Repositorio Remoto (Gitlab ó Git.ti):
	git clone git@git.ti:att/sistemas-mixtos/plataformasvirtuales.git
#### Listar Ramas
	git branch
	*(Para crear una nueva rama: git branch rama_de_trabajo)
#### Ubicarse en la "rama_de_trabajo"
	git checkout rama_de_trabajo
#### Para verificar el estado (antes de los commits)
	git status
#### Subir a la Memoria del GIT Local
	git add -A
#### Registrar los cambios realizados en el GIT Local
	git commit -m "NOMBRE_RAMA - Cambios realizados..."

## 3. Subir el Proyecto al Repositorio Remoto (Gitlab ó git.ti)
#### Subir los cambios al Repositorio Remoto
	git push origin rama_de_trabajo

## 4. Creación del Merge Request (Realizarlo en la Interfaz Gitlab ó Git.ti)
#### Ingresar al Repositorio Remoto y cada desarrollador realizará los "merge's" correspondientes deacuerdo su proyecto (Desmaracar "Delete Branch")

## 5. Actualizar el Proyecto del Repositorio Remoto GIT a un Servidor Producción/Desarrollo/Local
#### Para bajar los cambios ingresar al servidor de producción/desarrollo/local por ssh y luego ejecutar:
	git pull origin main
#### Para ver el Historial de Commits
	git log

## 6. Mantener actualizado el proyecto personal (Realizarlo de manera recurrente cada día)
#### Actualizar el Proyecto del Repositorio Remoto Gitlab ó Git.ti la rama "main" al proyecto local
	git checkout main
	git pull origin main
#### Actualizar la "rama_de_trabajo" de la rama "main"
	git checkout rama_de_trabajo
	git merge main